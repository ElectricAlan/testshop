json.array!(@items) do |item|
  json.extract! item, :id, :productID, :category, :name, :price
  json.url item_url(item, format: :json)
end
