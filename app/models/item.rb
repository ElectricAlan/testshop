require 'csv'

class Item < ActiveRecord::Base

  def self.import(file)
    csv_text = File.read(file.path)
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|

      item_hash = row.to_hash
      item = Item.where(productID: item_hash["ProductID"])

      category = Category.where(name: item_hash["Category"])

      unless category.count > 0 # create new category if it doesn't exist
        category = Category.create({ "name" => item_hash["Category"]})
      end

      params = {
        "productID" => item_hash["ProductID"],
        "category" => item_hash["Category"],
        "name" => item_hash["Product Name"],
        "price" => item_hash["Price"],
      }

      if item.count == 0 #check row doesn't exist
        Item.create(params)
      else
        item.first.update_attributes(params)
      end
    end
  end
end
