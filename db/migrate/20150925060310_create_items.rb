class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :productID
      t.string :category
      t.string :name
      t.decimal :price

      t.timestamps
    end
  end
end
